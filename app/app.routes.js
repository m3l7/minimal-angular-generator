(function(){
  angular
    .module('app')
    .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'pages/home.html',
          controller: 'Home',
        })
    });  
})();
