// Backend mocking with fake data

(function(){
    angular
        .module('app')
        .run(mockBackend);

    mockBackend.$inject = ['$httpBackend','testData','config'];

    function mockBackend($httpBackend,testData,config){
        // VIDEO
           $httpBackend.whenGET(/apiv1\/video/).respond(function(method,url,data){
               return [200,[],{}];
           })
        // DEFAULT
            $httpBackend.whenGET(/.*/).passThrough();
            $httpBackend.whenPOST(/.*/).passThrough();
            $httpBackend.whenPUT(/.*/).passThrough();
            $httpBackend.whenDELETE(/.*/).passThrough();
    }

})();