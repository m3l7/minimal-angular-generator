(function(){
    angular
        .module('app')
        .factory('VideoModel',videoModel);

        videoModel.$inject = ['DS'];

        function videoModel(DS){

            return DS.defineResource({
                name: 'video',
                relations:{
                    belongsTo:{
                        category:{
                            localField: 'category',
                            localKey: 'categoryId',
                        }
                    },
                    hasMany:{
                        relatedVideo:{
                            localField: 'relatedVideos',
                        }
                    }

                }
            });
        }
})();