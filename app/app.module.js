'use strict';

/**
 * @ngdoc overview
 * @name uauaoApp
 * @description
 * # uauaoApp
 *
 * Main module of the application.
 */
(function(){
  angular
    .module('app', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ui.bootstrap',
      'pascalprecht.translate',
      'ngMockE2E',
      'js-data',
      'angularMoment'
    ])
})();