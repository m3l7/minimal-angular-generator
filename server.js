var express = require('express')
    , http = require('http')
    , path = require('path')
    , app = express()

// all environments
app.set('port', process.env.PORT || 14002);

if (app.get('env')=='dist') app.use(express.static(path.join(__dirname, './dist')));
else if (app.get('env')=='prod') app.use(express.static(path.join(__dirname, './prod')));
else app.use(express.static(path.join(__dirname, './app')));

http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});